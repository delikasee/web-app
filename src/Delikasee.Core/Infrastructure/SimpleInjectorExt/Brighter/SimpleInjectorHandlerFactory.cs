﻿using System;
using Paramore.Brighter;
using SimpleInjector;

namespace Delikasee.Core.Infrastructure.SimpleInjectorExt.Brighter
{
    internal class SimpleInjectorHandlerFactory : IAmAHandlerFactory
    {
        private readonly Container container;

        public SimpleInjectorHandlerFactory(Container container)
        {
            this.container = container;
        }

        public IHandleRequests Create(Type handlerType)
        {
            return (IHandleRequests)container.GetInstance(handlerType);
        }

        // ReSharper disable once RedundantAssignment
        public void Release(IHandleRequests handler) => handler = null;
    }

    internal class SimpleInjectorHandlerFactoryAsync : IAmAHandlerFactoryAsync
    {
        private readonly Container container;

        public SimpleInjectorHandlerFactoryAsync(Container container)
        {
            this.container = container;
        }

        // ReSharper disable once RedundantAssignment
        public void Release(IHandleRequestsAsync handler) => handler = null;

        IHandleRequestsAsync IAmAHandlerFactoryAsync.Create(Type handlerType)
        {
            return (IHandleRequestsAsync)container.GetInstance(handlerType);
        }
    }
}
