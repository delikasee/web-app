﻿using Amazon.DynamoDBv2;
using DDD;
using Delikasee.Repository.DynamoDb;
using Microsoft.Extensions.Configuration;
using ServiceStack.Aws.DynamoDb;
using SimpleInjector;

namespace Delikasee.Core.Infrastructure.Repository
{
    //note: consider moving this out to lessen depenencies of Core module
    public static class DomainRepositorySetupHelper
    {
        public static void SetupDynamoDbRepository(this Container container, IConfiguration config)
        {
            var options = config.GetAWSOptions();
            var dynamoDb = options.CreateServiceClient<IAmazonDynamoDB>();

            var pocoDynamo = new PocoDynamo(dynamoDb);

            pocoDynamo.RegisterTable<DynamoDbEventAdapter>();

            pocoDynamo.InitSchema();

            var repository = new DynamoDbDomainRepository(pocoDynamo);
            container.RegisterInstance<IDomainRepository>(repository);
        }
    }
}
