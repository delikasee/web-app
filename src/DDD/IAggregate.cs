﻿using System;
using System.Collections.Generic;

namespace DDD
{
    public interface IAggregate
    {
        int Version { get; }
        Guid Id { get; }
        void ApplyEvent(object @event);
        ICollection<IDomainEvent> GetPendingEvents();
        void ClearPendingEvents();
    }
}
