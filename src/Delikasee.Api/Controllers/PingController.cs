﻿using Microsoft.AspNetCore.Mvc;

namespace Delikasee.Api.Controllers
{
    public class PingController : Controller
    {
        [HttpGet("ping")]
        public string Ping() => "pong";
    }
}
