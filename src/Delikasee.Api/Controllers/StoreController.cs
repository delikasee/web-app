﻿using System.Threading.Tasks;
using Delikasee.Store.Commands;
using Microsoft.AspNetCore.Mvc;
using Paramore.Brighter;

namespace Delikasee.Api.Controllers
{
#pragma warning disable S4144 // Methods should not have identical implementations
    [Route("store")]
    public class StoreController : Controller
    {
        private readonly IAmACommandProcessor commandProcessor;

        public StoreController(IAmACommandProcessor commandProcessor)
        {
            this.commandProcessor = commandProcessor;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateStore([FromBody]CreateStore command)
        {
            await commandProcessor.SendAsync(command);

            return Ok();
        }

        [HttpPost("change-name")]
        public async Task<IActionResult> ChangeStoreName([FromBody]ChangeStoreName command)
        {
            await commandProcessor.SendAsync(command);

            return Ok();
        }
    }
#pragma warning restore S4144 // Methods should not have identical implementations
}
