﻿using System;
using DDD;
using Delikasee.ProductCatalog.Events;

namespace Delikasee.ProductCatalog.Model
{
    public class Product : AggregateBase
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string PrimaryImage { get; private set; }

        /// <summary>
        /// Use constructor with parameter when creating a new domain object
        /// This parameterless constructor is for the repository
        /// </summary>
        [Obsolete("Use constructor with parameter when creating a new domain object")]
        public Product()
        {

        }

        public Product(Guid productId, string name)
        {
            RaiseEvent(new ProductCreated(productId, name));
        }

        public void Apply(ProductCreated e)
        {
            Id = e.AggregateId;
            Name = e.Name;
        }

        public void ChangeName(string name)
        {
            RaiseEvent(new ProductNameChanged(Id, name));
        }

        public void Apply(ProductNameChanged e)
        {
            Name = e.Name;
        }

        public void ChangeDescription(string description)
        {
            RaiseEvent(new ProductDescriptionChanged(Id, description));
        }

        public void Apply(ProductDescriptionChanged e)
        {
            Description = e.Description;
        }

        public void ChangePrimaryImage(string imageUrl)
        {
            RaiseEvent(new ProductPrimaryImageChanged(Id, imageUrl));
        }

        public void Apply(ProductPrimaryImageChanged e)
        {
            PrimaryImage = e.ImageUrl;
        }

        public override string Identifier => $"product-{Id}";
    }
}
