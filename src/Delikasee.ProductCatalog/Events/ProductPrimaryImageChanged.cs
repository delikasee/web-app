﻿using System;
using DDD;

namespace Delikasee.ProductCatalog.Events
{
    public class ProductPrimaryImageChanged : BaseDomainEvent
    {
        public string ImageUrl { get; set; }

        public ProductPrimaryImageChanged(Guid productId, string imageUrl)
        {
            AggregateId = productId;
            ImageUrl = imageUrl;
        }
    }
}
