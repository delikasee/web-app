﻿using System;
using DDD;

namespace Delikasee.ProductCatalog.Events
{
    public class ProductDescriptionChanged : BaseDomainEvent
    {
        public string Description { get; set; }

        public ProductDescriptionChanged(Guid productId, string description)
        {
            AggregateId = productId;
            Description = description;
        }
    }
}
