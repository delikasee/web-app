﻿using System;
using DDD;

namespace Delikasee.ProductCatalog.Events
{
    public class ProductNameChanged : BaseDomainEvent
    {
        public string Name { get; set; }

        public ProductNameChanged(Guid productId, string name)
        {
            AggregateId = productId;
            Name = name;
        }
    }
}
