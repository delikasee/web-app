﻿using System;
using DDD;

namespace Delikasee.Store.Events
{
    public class ScheduledItemSellingResumed : BaseDomainEvent
    {
        public ScheduledItemSellingResumed(Guid itemId)
        {
            AggregateId = itemId;
        }
    }
}
