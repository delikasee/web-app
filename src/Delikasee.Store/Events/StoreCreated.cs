﻿using System;
using DDD;

namespace Delikasee.Store.Events
{
    public class StoreCreated : BaseDomainEvent
    {
        public string Name { get; set; }
        public Guid OwnerId { get; set; }

        public StoreCreated(Guid storeId, Guid ownerId, string name)
        {
            AggregateId = storeId;
            OwnerId = ownerId;
            Name = name;
        }
    }
}
