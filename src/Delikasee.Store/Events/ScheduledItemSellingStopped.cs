﻿using System;
using DDD;

namespace Delikasee.Store.Events
{
    public class ScheduledItemSellingStopped : BaseDomainEvent
    {
        public ScheduledItemSellingStopped(Guid itemId)
        {
            AggregateId = itemId;
        }
    }
}
