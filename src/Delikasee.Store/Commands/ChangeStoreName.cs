﻿using System;
using Paramore.Brighter;

namespace Delikasee.Store.Commands
{
    public class ChangeStoreName : IRequest
    {
        public Guid Id { get; set; }
        public Guid StoreId { get; set; }
        public string Name { get; set; }
    }
}
