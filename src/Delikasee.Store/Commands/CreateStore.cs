﻿using System;
using Paramore.Brighter;

namespace Delikasee.Store.Commands
{
    public class CreateStore : IRequest
    {
        public Guid Id { get; set; }
        public Guid StoreId { get; set; }
        public Guid OwnerId { get; set; }
        public string Name { get; set; }
    }
}
