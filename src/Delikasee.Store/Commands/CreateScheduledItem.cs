﻿using System;
using Paramore.Brighter;

namespace Delikasee.Store.Commands
{
    public class CreateScheduledItem : IRequest
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public Guid StoreId { get; set; }
        public Guid ProductId { get; set; }
    }
}
