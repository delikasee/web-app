﻿using System;
using Paramore.Brighter;

namespace Delikasee.Store.Commands
{
    public class StopSellingScheduledItem : IRequest
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
    }
}
