﻿using System.Threading;
using System.Threading.Tasks;
using DDD;
using Delikasee.Store.Commands;
using Paramore.Brighter;

namespace Delikasee.Store.CommandHandlers
{
    public class ChangeStoreNameHandler : RequestHandlerAsync<ChangeStoreName>
    {
        private readonly IDomainRepository domainRepository;

        public ChangeStoreNameHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override async Task<ChangeStoreName> HandleAsync(ChangeStoreName command, CancellationToken cancellationToken = new CancellationToken())
        {
            var model = domainRepository.GetById<Model.Store>(command.StoreId);
            model.ChangeName(command.Name);
            domainRepository.Save(model, true);

            return await base.HandleAsync(command, cancellationToken)
                .ConfigureAwait(ContinueOnCapturedContext);
        }
    }
}
