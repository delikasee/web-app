﻿using System.Threading;
using System.Threading.Tasks;
using DDD;
using Delikasee.Store.Commands;
using Paramore.Brighter;

namespace Delikasee.Store.CommandHandlers
{
    public class ResumeSellingScheduledItemHandler : RequestHandlerAsync<ResumeSellingScheduledItem>
    {
        private readonly IDomainRepository domainRepository;

        public ResumeSellingScheduledItemHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override async Task<ResumeSellingScheduledItem> HandleAsync(ResumeSellingScheduledItem command, CancellationToken cancellationToken = new CancellationToken())
        {
            var model = domainRepository.GetById<Model.ScheduledItem>(command.ItemId);
            model.ResumeSelling();
            domainRepository.Save(model, true);

            return await base.HandleAsync(command, cancellationToken)
                .ConfigureAwait(ContinueOnCapturedContext);
        }
    }
}
