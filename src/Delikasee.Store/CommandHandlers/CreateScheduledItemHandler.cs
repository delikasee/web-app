﻿using System.Threading;
using System.Threading.Tasks;
using DDD;
using Delikasee.Store.Commands;
using Paramore.Brighter;

namespace Delikasee.Store.CommandHandlers
{
    public class CreateScheduledItemHandler : RequestHandlerAsync<CreateScheduledItem>
    {
        private readonly IDomainRepository domainRepository;

        public CreateScheduledItemHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override async Task<CreateScheduledItem> HandleAsync(CreateScheduledItem command, CancellationToken cancellationToken = new CancellationToken())
        {
            var model = new Model.ScheduledItem(command.ItemId, command.StoreId, command.ProductId);
            domainRepository.Save(model, true);

            return await base.HandleAsync(command, cancellationToken)
                .ConfigureAwait(ContinueOnCapturedContext);
        }
    }
}
