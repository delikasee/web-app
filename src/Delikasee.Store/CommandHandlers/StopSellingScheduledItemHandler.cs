﻿using System.Threading;
using System.Threading.Tasks;
using DDD;
using Delikasee.Store.Commands;
using Paramore.Brighter;

namespace Delikasee.Store.CommandHandlers
{
    public class StopSellingScheduledItemHandler : RequestHandlerAsync<StopSellingScheduledItem>
    {
        private readonly IDomainRepository domainRepository;

        public StopSellingScheduledItemHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override async Task<StopSellingScheduledItem> HandleAsync(StopSellingScheduledItem command, CancellationToken cancellationToken = new CancellationToken())
        {
            var model = domainRepository.GetById<Model.ScheduledItem>(command.ItemId);
            model.StopSelling();
            domainRepository.Save(model, true);

            return await base.HandleAsync(command, cancellationToken)
                .ConfigureAwait(ContinueOnCapturedContext);
        }
    }
}

