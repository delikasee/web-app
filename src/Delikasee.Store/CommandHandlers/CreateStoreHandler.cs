﻿using System.Threading;
using System.Threading.Tasks;
using DDD;
using Delikasee.Store.Commands;
using Paramore.Brighter;

namespace Delikasee.Store.CommandHandlers
{
    public class CreateStoreHandler : RequestHandlerAsync<CreateStore>
    {
        private readonly IDomainRepository domainRepository;

        public CreateStoreHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override async Task<CreateStore> HandleAsync(CreateStore command, CancellationToken cancellationToken = new CancellationToken())
        {
            var model = new Model.Store(command.StoreId, command.OwnerId, command.Name);
            domainRepository.Save(model, true);

            return await base.HandleAsync(command, cancellationToken)
                .ConfigureAwait(ContinueOnCapturedContext);
        }
    }
}
