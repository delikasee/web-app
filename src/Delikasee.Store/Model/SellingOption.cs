﻿using System;
using System.ComponentModel;

namespace Delikasee.Store.Model
{
    public class SellingOption
    {
        public StartSellingOption StartSellingOption { get; set; }
        public StopSellingOption StopSellingOption { get; set; }

        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
    }

    public enum StartSellingOption
    {
        [Description("At a specific time")] OnDate = -1,
        [Description("Now")] Now = 0
    }

    public enum StopSellingOption
    {
        [Description("At a specific time")] OnDate = -1,
        [Description("1 day before")] OneDay = 1440,
        [Description("2 days before")] TwoDays = 2880,
        [Description("3 days before")] ThreeDays = 4320,
        [Description("4 days before")] FourDays = 5760,
        [Description("5 days before")] FiveDays = 7200,
        [Description("6 days before")] SixDays = 8640,
        [Description("7 days before")] SevenDays = 10080
    }
}
