﻿using System;
using Amazon.DynamoDBv2.DataModel;
using ServiceStack.DataAnnotations;

namespace Delikasee.Repository.DynamoDb
{
    //this can be used by other aggregates 
    [Alias("Delikasee")] //[DynamoDBTable attribute not working for POCODynamo, using Alias[]
    public class DynamoDbEventAdapter
    {
        [DynamoDBHashKey]
        public string AggregateId { get; set; }

        [DynamoDBRangeKey]
        public int Version { get; set; }

        public DateTimeOffset TimeStamp { get; set; }

        public string EventData { get; set; }

        public string EventType { get; set; }
    }
}
