﻿using System;
using System.Linq;
using Amazon.DynamoDBv2;
using DDD;
using ServiceStack;
using ServiceStack.Aws.DynamoDb;

namespace Delikasee.Repository.DynamoDb
{
    public class DynamoDbDomainRepository : DomainRepositoryBase
    {
        private readonly IPocoDynamo pocoDynamo;

        public DynamoDbDomainRepository(IAmazonDynamoDB dynamoDb)
        {
            this.pocoDynamo = new PocoDynamo(dynamoDb);
        }

        //for direct mock testing
        public DynamoDbDomainRepository(IPocoDynamo pocoDynamo)
        {
            this.pocoDynamo = pocoDynamo;
        }

        public override void Save<TAggregate>(TAggregate aggregate, bool isInitial = false)
        {
            var pendingEvents = aggregate.GetPendingEvents().ToList();

            if (!pendingEvents.Any())
                return;

            var originalVersion = aggregate.Version - pendingEvents.Count + 1;

            var eventList = pendingEvents
                .Select(pendingEvent => new DynamoDbEventAdapter
                {
                    AggregateId = aggregate.Id.ToString(),
                    TimeStamp = pendingEvent.TimeStamp,
                    Version = originalVersion++,
                    EventData = pendingEvent.ToJson(),
                    EventType = pendingEvent.GetType().Name
                })
                .ToList();

            eventList.ForEach(e =>
            {
                pocoDynamo.PutItem(e);
            });


            aggregate.ClearPendingEvents();
        }

        public override TAggregate GetById<TAggregate>(Guid id)
        {
            var @events = pocoDynamo.FromQuery<DynamoDbEventAdapter>(x => x.AggregateId == id.ToString())
                .Exec().ToList();

            var deserializedEvents = @events.Select(x => (IDomainEvent)x.EventData.FromJson<object>()).ToList();

            var aggregate = BuildAggregate<TAggregate>(deserializedEvents);
            return aggregate;
        }

        public override TAggregate GetById<TAggregate>(Guid id, int version)
        {
            throw new NotImplementedException();
        }
    }
}

